import express from "express";
import morgan from "morgan";
import {createProxyMiddleware} from "http-proxy-middleware";

export class NodeServerProxy {

    constructor(options) {
        this.nodeServerProxyOptions = options;
        this.nodeServerProxy = express();
    }

    start() {
        this.processConfigProxies();
        this.processParamProxy();
        this.nodeServerProxy.use(morgan('combined'));
        this.processStaticUrl();
        this.nodeServerProxy.listen(this.nodeServerProxyOptions.port);
        console.log(`Started node server on http://localhost:${this.nodeServerProxyOptions.port}`);
    }

    processStaticUrl() {
        this.nodeServerProxy.use(express.static(this.nodeServerProxyOptions._[0]));
        this.nodeServerProxy.get('*', (req, res) => {
            res.sendFile('index.html', {root: this.nodeServerProxyOptions._[0]});
        });
    }

    processParamProxy() {
        const target = this.nodeServerProxyOptions.proxy;
        if (target) {
            if (this.nodeServerProxyOptions.config) {
                console.log("Config file attached. Ignore proxy argument.");
                return;
            }
            console.log(`Attaching target ${target} to path /`);
            this.nodeServerProxy.use(`/`, createProxyMiddleware({target}));
        }
    }

    processConfigProxies() {
        if (this.nodeServerProxyOptions.proxies) {
            this.nodeServerProxyOptions.proxies.forEach((proxy) => {
                console.log(`Attaching target ${proxy.target} to path ${proxy.path}`);
                this.nodeServerProxy.use(proxy.path, createProxyMiddleware({target: proxy.target}));
            })
        }
    }
}