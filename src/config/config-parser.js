import fs from "fs";

export class ConfigParser {
    static parseConfig(configPath) {
        let rawJSONString = fs.readFileSync(configPath, 'utf-8');
        const variables= rawJSONString.match(/\${.*?}/g);
        variables.forEach(value => {
            const noBracketsValue = value.replace(/[${}]/g, '');
            const envVariableSplitted = noBracketsValue.split(`:-`);
            rawJSONString = rawJSONString.replace(value, process.env[envVariableSplitted[0]] || envVariableSplitted[1]);
        });
        return JSON.parse(rawJSONString)
    }
}