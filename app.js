#!/usr/bin/env node

import yargs from "yargs";
import {NodeServerProxy} from "./src/node-server-proxy.js";
import {ConfigParser} from "./src/config/config-parser.js";

const options = yargs
.usage("Usage: -n <name>")
.config("config", "Proxy configuration file. If specified - proxy (-p) is ignored.", ConfigParser.parseConfig)
.option("P", {alias: "proxy", describe: "Simple proxy configuration - all request will be proxied", type: "string"})
.option("p", {alias: "port", describe: "Port of server instance", default: 3000, type: "number"})
    .command("path", "Path to served static content")
    .argv;

new NodeServerProxy(options).start();
