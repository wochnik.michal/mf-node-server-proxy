# MF Node Server Proxy
This node server proxy is set on express, and it's basing on [http-proxy-middleware](https://github.com/chimurai/http-proxy-middleware).

## Install
You can install it as npm package in your project:

`npm install mf-node-server-proxy`

Or install it globally to use in command line:

`npm install -g mf-node-server-proxy`

## Using
### Command line

Use it is as simple as:

`mf-node-server-proxy -p 3000 -P http://localhost:8080 ./build`

or if you want to use more advanced proxy config:

`mf-node-server-proxy -p 3000 --config ./config.json ./build`

Where config.json looks like:

```json
{
  "proxies": [
    {
      "path": "/api",
      "target": "https://www.google.pl"
    }
  ]
}
```